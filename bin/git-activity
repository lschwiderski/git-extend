#!/bin/bash

# Copyright 2019 Lucas Schwiderski
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

GIT_OPTS=("")
format="%(refname:lstrip=2) (%(authordate:relative)): [%(authorname)] %(contents:subject)"

# actually parse the options and do stuff
while [[ $1 = -?* ]]; do
    case $1 in
        -h|--help)
            man git-activity
            ;;
        -c|--count)
            shift
            GIT_OPTS+=("--count=$1")
            ;;
        --no-tag)
            exclude_tag=1
            ;;
        -r|--remote)
            shift
            remote="$1"
            ;;
        --format)
            shift
            format="$1"
            ;;
        *) ;;
    esac

shift
done

# TODO: Add a count to change how many tags are displayed -> default 1

# TODO: Use custom logging format:
# ref_name (date): [author] message

if [ -z "$exclude_tag" ]; then
    echo "Recently created tag:"
    git for-each-ref ${GIT_OPTS} --sort="-authordate" --count=1 --format="$format" refs/tags
    echo ""
fi

if [ -n "$remote" ]; then
    refs="refs/remotes/$remote"
else
    refs="refs/heads"
fi

echo "Recently updated branches:"
git for-each-ref ${GIT_OPTS} --sort="-authordate" --format="$format" "$refs"

